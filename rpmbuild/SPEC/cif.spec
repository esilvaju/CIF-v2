Summary:        CIF-V2
Name:           massive-octo-spice
Version:        2.0
Release:        0
License:        GPL
Group:          System Environment/Applicatio
URL:            https://github.com/csirtgadgets/massive-octo-spice
Packager:       Eraldo Silva Junior
BuildRoot:      %{_tmppath}/%{name}-{%version}.{%release}
Source0:        %{name}-%version-%release.x86_64.tar.gz
BuildArch: 	x86_64
Requires: python, curl, monit, GeoIP, GeoIP-update, perl-CPAN, perl-CGI-Application, libssl-dev, openssl-libs, mailutils, java-1.8.0-openjdk, rng-tools, java-1.8.0-openjdk-headless, libtool.x86_64, zeromq3, zeromq3-devel, libffi-devel.x86_64, ibffi, perl-Moose, perl-AnyEvent, GeoIP-devel, python-GeoIP, python-geoip-geolite2, python-devel, bind, perl-App-cpanminus, httpd, mod_ssl, perl-libapreq2, libxml2-devel

%description
massive-octo-spice

%prep
#uncompress and get into directory
%setup -n massive-octo-spice

%build
# Check dependecies and prepare for install
./configure --enable-geoip --sysconfdir=/etc/cif --localstatedir=/var --prefix=/opt/cif
make && make deps NOTESTS=-n
make test

%install
make install

%post
#post script 
./post.sh

%files
#Default permissions
%defattr(-,root,root,-)
#Directories will be placed into the binary RPM. 
/opt/cif/*
%{_localstatedir}/cache/*
%{_sysconfdir}/cif/*

%changelog
  Thu Nov 15 2015 Eraldo Jr <ejunior@cern.ch> - 1.0
- Initial spec file setup.
