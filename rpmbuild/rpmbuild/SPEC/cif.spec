Summary:        CIF-V2
Name:           massive-octo-spice
Version:        2.0
Release:        0
License:        GPL
Group:          System Environment/Applicatio
URL:            https://github.com/csirtgadgets/massive-octo-spice
Packager:       Eraldo Silva Junior
BuildRoot:      %{_tmppath}/%{name}-{%version}.{%arch}
Source0:        %{name}-%version.tar.gz
BuildArch: 	x86_64
Requires: python, curl, monit, GeoIP, GeoIP-update, perl-CPAN, perl-CGI-Application, libssl-dev, openssl-libs, mailutils, java-1.8.0-openjdk, rng-tools, java-1.8.0-openjdk-headless, libtool.x86_64, zeromq3, zeromq3-devel, libffi-devel.x86_64, ibffi, perl-Moose, perl-AnyEvent, GeoIP-devel, python-GeoIP, python-geoip-geolite2, python-devel, bind, perl-App-cpanminus, httpd, mod_ssl, perl-libapreq2, libxml2-devel

%description
massive-octo-spice

%prep
%setup -q -n massive-octo-spice

(cd $RPM_BUILD_ROOT;./prep.sh)

%build
./configure --enable-geoip --sysconfdir=/etc/cif --localstatedir=/var --prefix=/opt/cif
make && make deps NOTESTS=-n
make test

%install
make install
rm -rf $RPM_BUILD_ROOT

%post
./post.sh

%files
%defattr(-,root,root,-)
$RPM_BUILD_ROOT/massive-octo-spice/*

%changelog
* Thu Nov 15 2015 Eraldo Jr <ejunior@cern.ch> - 1.0
- Initial spec file setup.
